﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using DataBaseManager;
using DataBaseManager.Models;
using Newtonsoft.Json;
using WebModels;
using Jint;

namespace MockREST.Controllers
{
    [RoutePrefix("execute")]
    [Route("{action=execute}")]
    public class ExecuteController : Controller
    {
        [HttpGet]
        [Route("{endpointLink:regex(\\w+)}")]
        [ActionName("execute")]
        public ActionResult ExecuteGet(string endpointLink)
        {
            return Execute(endpointLink, "get");
        }

        [HttpPost]
        [Route("{endpointLink:regex(\\w+)}")]
        [ActionName("execute")]
        public ActionResult ExecutePost(string endpointLink)
        {
            return Execute(endpointLink, "post");
        }

        [HttpPut]
        [Route("{endpointLink:regex(\\w+)}")]
        [ActionName("execute")]
        public ActionResult ExecutePut(string endpointLink)
        {
            return Execute(endpointLink, "put");
        }

        [HttpDelete]
        [Route("{endpointLink:regex(\\w+)}")]
        [ActionName("execute")]
        public ActionResult ExecuteDelete(string endpointLink)
        {
            return Execute(endpointLink, "delete");
        }

        private ActionResult Execute(string endpointLink, string method)
        {
            DataBaseContext db = new DataBaseContext();

            Endpoint e = db.Endpoints.FirstOrDefault(x => x.Link == endpointLink);
            if (e == null || e.HttpMethod != method || e.IsEnabled == false)
                return HttpNotFound();

            if (e.AuthToken != null)
            {
                if (Request.Headers["Auth-Token"] == null)
                {
                    Response.StatusCode = 403;
                    return Content("No Auth-Token");
                }

                string givenToken = Request.Headers["Auth-Token"];
                if (e.AuthToken != givenToken)
                {
                    Response.StatusCode = 403;
                    return Content("Auth-Token Mismatch");
                }
            }

            Stopwatch s = new Stopwatch();
            s.Start();

            if (e.Delay.TotalMilliseconds > 0)
                Thread.Sleep(e.Delay.Milliseconds);

            Response.StatusCode = e.ReturnCode;

            string result = e.IsStaticContent ? e.Content : ExecuteJS(e.Content, HeadersToKvplList(HttpUtility.ParseQueryString(Request.Url.Query)));

            s.Stop();

            History h = new History();
            h.TimeStamp = DateTime.UtcNow;
            h.EndpointId = e.Id;
            h.HttpStatus = Response.StatusCode;
            h.RequestUrl = Request.Url.PathAndQuery;
            h.RequestHeader = JsonConvert.SerializeObject(HeadersToKvplList(Request.Headers));
            h.ResponseText = result;
            h.ResponseHeader = JsonConvert.SerializeObject(HeadersToKvplList(Response.Headers));
            h.ExecutionTime = s.Elapsed;

            db.Histories.Add(h);
            db.SaveChanges();

            return Content(result);
        }

        private List<KeyValuePair> HeadersToKvplList(NameValueCollection header)
        {
            return header.AllKeys.Select(key => new KeyValuePair {Key = key, Value = header[key]}).ToList();
        }

        private string ExecuteJS(string jsCode, List<KeyValuePair> data)
        {
            var dataObj = new Dictionary<string, object>();
            foreach (KeyValuePair item in data)
                dataObj[item.Key] = item.Value;
            
            string json = JsonConvert.SerializeObject(dataObj);

            string code = "var result = \"\"; var data = JSON.parse(dataJSON);" + Environment.NewLine + jsCode;

            string result = "";
            try
            {
                result = new Engine()
                            .SetValue("dataJSON", json)
                            .Execute(code)
                            .GetValue("result")
                            .ToString();
            }
            catch (Exception ex)
            {
                result = "JavaScript Error!" + Environment.NewLine + ex.Message;
            }
            return result;
        }
    }
}