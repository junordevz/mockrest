﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataBaseManager;
using DataBaseManager.Models;
using Newtonsoft.Json;
using SecurityModule;
using WebModels;
using WebModels.History;

namespace MockREST.Controllers.API
{
    [ApiActionFilter]
    [RoutePrefix("api/history")]
    public class HistoryController : Controller
    {
        [Route("ofendpoint")]
        public ActionResult OfEndpoint()
        {
            HistoryOfEndpointResponse resp = new HistoryOfEndpointResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["link"] == null || qParams["skip"] == null || qParams["take"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string link = qParams["link"];
                string skip = qParams["skip"];
                string take = qParams["take"];

                int skipN, takeN;
                if (int.TryParse(skip, out skipN) == false ||
                    int.TryParse(take, out takeN) == false ||
                    skipN < 0 || takeN < 0)
                    resp.StatusCode = StatusCodes.InvalidParams;
                else
                {
                    DataBaseContext db = new DataBaseContext();
                    Endpoint e = db.Endpoints.FirstOrDefault(x => x.Link == link);
                    if (e == null)
                        resp.StatusCode = StatusCodes.EndpointNotFound;
                    else
                    {
                        long endpointId = e.Id;
                        resp.Entries.AddRange(
                            db.Histories.Where(x => x.EndpointId == endpointId)
                                        .OrderByDescending(x => x.TimeStamp)
                                        .Skip(skipN)
                                        .Take(takeN)
                                        .ToList()
                                        .Select(x => new HistoryEntry
                                        {
                                            Id = x.Id,
                                            TimeStamp = x.TimeStamp,
                                            HttpStatus = x.HttpStatus,
                                            ExecTimeMs = Convert.ToInt64(x.ExecutionTime.TotalMilliseconds)
                                        }));
                    }
                }
            }

            return Content(JsonConvert.SerializeObject(resp));
        }

        [Route("details")]
        public ActionResult Details()
        {
            HistoryDetailsResponse resp = new HistoryDetailsResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            long historyId = 0;
            if (qParams["id"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
                if (long.TryParse(qParams["id"], out historyId) == false)
                    resp.StatusCode = StatusCodes.InvalidParams;
                else
                {
                    DataBaseContext db = new DataBaseContext();

                    History h = db.Histories.FirstOrDefault(x => x.Id == historyId);
                    if (h == null)
                        resp.StatusCode = StatusCodes.HistoryNotFound;
                    else
                    {
                        resp.Properties.TimeStamp = h.TimeStamp;
                        resp.Properties.HttpStatus = h.HttpStatus;
                        resp.Properties.RequestUrl = h.RequestUrl;
                        resp.Properties.RequestHeaders = JsonConvert.DeserializeObject<List<KeyValuePair>>(h.RequestHeader);
                        resp.Properties.ResponseText = h.ResponseText;
                        resp.Properties.ResponseHeaders = JsonConvert.DeserializeObject<List<KeyValuePair>>(h.ResponseHeader);
                        resp.Properties.ExecTime = Convert.ToInt64(h.ExecutionTime.TotalMilliseconds);
                    }
                }
                
            return Content(JsonConvert.SerializeObject(resp));
        }
    }
}