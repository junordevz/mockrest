﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataBaseManager;
using DataBaseManager.Models;
using Microsoft.Owin.Security.Infrastructure;
using Newtonsoft.Json;
using WebModels;
using WebModels.User;
using SecurityHelper = SecurityModule.Helper.SecurityHelper;

namespace MockREST.Controllers.API
{
    [RoutePrefix("api/users")]
    public class UserController : Controller
    {

        [Route("register")]
        public ActionResult Register()
        {
            UserRegisterResponse resp = new UserRegisterResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["user"] == null || qParams["password"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string name = qParams["user"];
                string pass = qParams["password"];

                if (SecurityHelper.UserExists(name))
                    resp.StatusCode = StatusCodes.UserExists;
                else
                {
                    SecurityHelper.Register(name, pass);
                }
            }
            return Content(JsonConvert.SerializeObject(resp));
        }

        [Route("login")]
        public ActionResult LogIn()
        {
            UserLogInResponse resp = new UserLogInResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["user"] == null || qParams["password"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string name = qParams["user"];
                string pass = qParams["password"];

                if (SecurityHelper.UserExists(name))
                    if (SecurityHelper.DoesPassMatch(name, pass))
                        resp.AuthToken = SecurityHelper.LogIn(name).ToString();
                    else
                        resp.StatusCode = StatusCodes.PassNoMatch;
                else
                    resp.StatusCode = StatusCodes.UserNotFound;
            }
            return Content(JsonConvert.SerializeObject(resp));
        }

        [Route("logout")]
        public ActionResult LogOut()
        {
            UserLogOutResponse resp = new UserLogOutResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["auth-token"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string token = qParams["auth-token"];
                DataBaseContext db = new DataBaseContext();
                User u = db.Users.FirstOrDefault(x => x.AuthToken == token);
                if (u == null)
                    resp.StatusCode = StatusCodes.UserNotFound;
                else
                    SecurityHelper.LogOut(u.Name);
                    
            }
            return Content(JsonConvert.SerializeObject(resp));
        }

    }
}