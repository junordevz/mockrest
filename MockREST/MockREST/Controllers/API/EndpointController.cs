﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataBaseManager;
using DataBaseManager.Models;
using Newtonsoft.Json;
using SecurityModule;
using WebModels;
using WebModels.Endpoint;

namespace MockREST.Controllers.API
{
    [ApiActionFilter]
    [RoutePrefix("api/endpoints")]
    public class EndpointController : Controller
    {
        [Route("create")]
        public ActionResult Create()
        {
            DataBaseContext db = new DataBaseContext();
            EndpointCreateStatus resp = new EndpointCreateStatus();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["user"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string user = qParams["user"];
                User u = db.Users.FirstOrDefault(x => x.Name == user);
                if (u == null)
                    resp.StatusCode = StatusCodes.UserNotFound;
                else
                {
                    string link;
                    do
                    {
                        link = Guid.NewGuid().ToString("n");
                    } while (db.Endpoints.Any(x => x.Link == link));
                    resp.Link = link;

                    Endpoint e = new Endpoint();
                    e.Link = link;
                    e.ProjectId = db.Projects.Where(x => x.OwnerId == u.Id).First(x => x.IsDefault).Id;
                    e.HttpMethod = "get";
                    e.ReturnCode = 200;
                    e.Delay = TimeSpan.FromSeconds(0);
                    e.IsStaticContent = true;
                    e.Content = "ok";
                    e.Description = "No Description";
                    e.IsEnabled = true;

                    db.Endpoints.Add(e);
                    db.SaveChanges();
                }
            }
            return Content(JsonConvert.SerializeObject(resp));
        }

        [Route("getendpoint")]
        public ActionResult GetEndpoint()
        {
            DataBaseContext db = new DataBaseContext();
            EndpointGetDetailsResponse resp = new EndpointGetDetailsResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["link"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string link = qParams["link"];
                Endpoint e = db.Endpoints.FirstOrDefault(x => x.Link == link);
                if (e == null)
                    resp.StatusCode = StatusCodes.EndpointNotFound;
                else
                {
                    resp.Properties.Link = link;
                    resp.Properties.ProjectId = e.ProjectId;
                    resp.Properties.ProjectName = e.Project.Name;
                    resp.Properties.HttpMethod = e.HttpMethod;
                    resp.Properties.HttpStatus = e.ReturnCode;
                    resp.Properties.DelayMs = Convert.ToInt32(e.Delay.TotalMilliseconds);
                    resp.Properties.IsStaticContent = e.IsStaticContent;
                    resp.Properties.Content = e.Content;
                    resp.Properties.Description = e.Description;
                    resp.Properties.AuthToken = e.AuthToken;
                    resp.Properties.IsEnabled = e.IsEnabled;
                }
            }

            return Content(JsonConvert.SerializeObject(resp));
        }

        [HttpPost]
        [Route("setendpoint")]
        public ActionResult SetEndpoint()
        {
            EndpointSetDetailsResponse resp = new EndpointSetDetailsResponse();
            resp.StatusCode = StatusCodes.Ok;

            string data = Request.Headers["data"];
            if (data == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                EndpointProperties endpoint = null;
                try
                {
                    endpoint = JsonConvert.DeserializeObject<EndpointProperties>(data);
                }
                catch (Exception)
                {
                    resp.StatusCode = StatusCodes.JsonInvalid;
                }

                if (endpoint != null)
                {
                    DataBaseContext db = new DataBaseContext();
                    Endpoint e = db.Endpoints.FirstOrDefault(x => x.Link == endpoint.Link);
                    if (e == null)
                        resp.StatusCode = StatusCodes.EndpointNotFound;
                    else
                    {
                        Project p = db.Projects.FirstOrDefault(x => x.Id == endpoint.ProjectId);
                        if (p == null)
                            resp.StatusCode = StatusCodes.ProjectNotFound;
                        else
                        {
                            e.ProjectId = endpoint.ProjectId;
                            e.HttpMethod = endpoint.HttpMethod;
                            e.ReturnCode = endpoint.HttpStatus;
                            e.Delay = TimeSpan.FromMilliseconds(endpoint.DelayMs);
                            e.IsStaticContent = endpoint.IsStaticContent;
                            e.Content = endpoint.Content;
                            e.AuthToken = endpoint.AuthToken;
                            e.Description = endpoint.Description;
                            e.IsEnabled = endpoint.IsEnabled;

                            db.SaveChanges();
                        }
                    }
                }
            }
            return Content(JsonConvert.SerializeObject(resp));
        }

        [HttpPost]
        [Route("setenable")]
        public ActionResult SeEnable()
        {
            EndpointSetEnabledResponse resp = new EndpointSetEnabledResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["link"] == null || qParams["enabled"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string link = qParams["link"];
                string enable = qParams["enabled"];
                bool enableFlag;
                if (bool.TryParse(enable, out enableFlag) == false)
                    resp.StatusCode = StatusCodes.InvalidParams;
                else
                {
                    DataBaseContext db = new DataBaseContext();
                    Endpoint e = db.Endpoints.FirstOrDefault(x => x.Link == link);
                    if (e == null)
                        resp.StatusCode = StatusCodes.ProjectNotFound;
                    else
                    {
                        e.IsEnabled = enableFlag;

                        db.SaveChanges();
                    }
                }
            }

            return Content(JsonConvert.SerializeObject(resp));
        }

        [Route("ofuser")]
        public ActionResult OfUser()
        {
            EndpoitOfUserResponse resp = new EndpoitOfUserResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["user"] == null || qParams["skip"] == null || qParams["take"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string user = qParams["user"];
                string skip = qParams["skip"];
                string take = qParams["take"];

                int skipN, takeN;
                if (int.TryParse(skip, out skipN) == false ||
                    int.TryParse(take, out takeN) == false ||
                    skipN < 0 || takeN < 0)
                    resp.StatusCode = StatusCodes.InvalidParams;
                else
                {
                    DataBaseContext db = new DataBaseContext();
                    User u = db.Users.FirstOrDefault(x => x.Name == user);
                    if (u == null)
                        resp.StatusCode = StatusCodes.UserNotFound;
                    else
                        resp.Elements.AddRange(
                            db.Endpoints.Where(x => x.Project.OwnerId == u.Id)
                                        .Skip(skipN)
                                        .Take(takeN)
                                        .Select(x => new EndpointElement
                                        {
                                            Link = x.Link,
                                            ProjectId = x.ProjectId,
                                            ProjectName = x.Project.Name,
                                            HttpMethod = x.HttpMethod,
                                            Description = x.Description,
                                            IsEnabled = x.IsEnabled
                                        }));
                }
            }
            return Content(JsonConvert.SerializeObject(resp));
        }

        [Route("ofproject")]
        public ActionResult OfProject()
        {
            EndpoitOfProjectResponse resp = new EndpoitOfProjectResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["id"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string project = qParams["id"];
                long projectId = 0;
                if (long.TryParse(project, out projectId) == false)
                    resp.StatusCode = StatusCodes.InvalidParams;
                else
                {
                    DataBaseContext db = new DataBaseContext();
                    Project p = db.Projects.FirstOrDefault(x => x.Id == projectId);
                    if (p == null)
                        resp.StatusCode = StatusCodes.ProjectNotFound;
                    else
                        resp.Elements.AddRange(
                            db.Endpoints.Where(x => x.ProjectId == projectId)
                                        .Select(x => new EndpointElement
                                        {
                                            Link = x.Link,
                                            ProjectId = x.ProjectId,
                                            ProjectName = x.Project.Name,
                                            HttpMethod = x.HttpMethod,
                                            Description = x.Description,
                                            IsEnabled = x.IsEnabled
                                        }));
                }
            }
            return Content(JsonConvert.SerializeObject(resp));
        }
    }
}