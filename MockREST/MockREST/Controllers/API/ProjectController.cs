﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataBaseManager;
using DataBaseManager.Models;
using Newtonsoft.Json;
using SecurityModule;
using WebModels;
using WebModels.Endpoint;
using WebModels.Project;

namespace MockREST.Controllers.API
{
    [ApiActionFilter]
    [RoutePrefix("api/projects")]
    public class ProjectController : Controller
    {
        [Route("create")]
        public ActionResult Create()
        {
            DataBaseContext db = new DataBaseContext();
            ProjectCreateStatus resp = new ProjectCreateStatus();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["user"] == null || qParams["name"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string user = qParams["user"];
                User u = db.Users.FirstOrDefault(x => x.Name == user);
                if (u == null)
                    resp.StatusCode = StatusCodes.UserNotFound;
                else
                {
                    string name = qParams["name"];

                    Project p = new Project();
                    p.OwnerId = u.Id;
                    p.Name = name;
                    p.IsDefault = false;

                    db.Projects.Add(p);
                    db.SaveChanges();
                }
            }
            return Content(JsonConvert.SerializeObject(resp));
        }

        [HttpPost]
        [Route("rename")]
        public ActionResult SetEndpoint()
        {
            ProjectRenameResponse resp = new ProjectRenameResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["id"] == null || qParams["name"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string name = qParams["name"];
                string id = qParams["id"];
                long projectId = 0;
                if (long.TryParse(id, out projectId) == false)
                    resp.StatusCode = StatusCodes.InvalidParams;
                else
                {
                    DataBaseContext db = new DataBaseContext();
                    Project p = db.Projects.FirstOrDefault(x => x.Id == projectId);
                    if (p == null)
                        resp.StatusCode = StatusCodes.ProjectNotFound;
                    else
                        if (p.IsDefault)
                            resp.StatusCode = StatusCodes.CannotRenameDefault;
                        else
                        {
                            p.Name = name;

                            db.SaveChanges();
                        }
                }
            }

            return Content(JsonConvert.SerializeObject(resp));
        }

        [Route("ofuser")]
        public ActionResult OfUser()
        {
            ProjectOfUserResponse resp = new ProjectOfUserResponse();
            resp.StatusCode = StatusCodes.Ok;

            var qParams = HttpUtility.ParseQueryString(Request.Url.Query);
            if (qParams["user"] == null)
                resp.StatusCode = StatusCodes.InvalidParams;
            else
            {
                string user = qParams["user"];
                DataBaseContext db = new DataBaseContext();
                User u = db.Users.FirstOrDefault(x => x.Name == user);
                if (u == null)
                    resp.StatusCode = StatusCodes.UserNotFound;
                else
                    resp.Elements.AddRange(
                        db.Projects.Where(x => x.OwnerId == u.Id)
                                    .Select(x => new ProjectElement
                                    {
                                         Id = x.Id,
                                         Name = x.Name,
                                         IsDefault = x.IsDefault
                                    }));            
            }

            return Content(JsonConvert.SerializeObject(resp));
        }
    }
}