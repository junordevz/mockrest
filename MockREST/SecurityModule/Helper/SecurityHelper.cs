﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseManager;
using DataBaseManager.Models;
using SecurityModule.Exceptions;

namespace SecurityModule.Helper
{
    public static class SecurityHelper
    {

        public static bool IsLogInExpired(string name, TimeSpan maxTime, DataBaseContext db = null)
        {
            if (db == null) db = new DataBaseContext();

            User u = db.Users.FirstOrDefault(x => x.Name == name);
            if (u == null)
                throw new UserDoeNoeExistException();

            return DateTime.Now.Subtract(u.LoginDate) > maxTime;
        }

        public static bool DoesPassMatch(string name, string givenPass, DataBaseContext db = null)
        {
            if (db == null) db = new DataBaseContext();

            User u = db.Users.FirstOrDefault(x => x.Name == name);
            if (u == null)
                throw new UserDoeNoeExistException();

            return PasswordHelper.HashPass(givenPass, u.PassSalt) == u.PassHash;
        }

        public static Guid LogIn(string name, DataBaseContext db = null)
        {
            if (db == null) db = new DataBaseContext();

            User u = db.Users.FirstOrDefault(x => x.Name == name);
            if (u == null)
                throw new UserDoeNoeExistException();

            Guid userGuid = Guid.NewGuid();
            u.AuthToken = userGuid.ToString();
            u.LoginDate = DateTime.Now;

            db.SaveChanges();

            return userGuid;
        }

        public static void LogOut(string name, DataBaseContext db = null)
        {
            if (db == null) db = new DataBaseContext();

            User u = db.Users.FirstOrDefault(x => x.Name == name);
            if (u == null)
                throw new UserDoeNoeExistException();

            u.AuthToken = "*";

            db.SaveChanges();
        }

        public static bool UserExists(string name, DataBaseContext db = null)
        {
            if (db == null) db = new DataBaseContext();

            return db.Users.Any(x => x.Name == name);
        }

        public static void Register(string name, string password)
        {
            DataBaseContext db = new DataBaseContext();

            if (UserExists(name, db))
                throw new UserAlreadyExistsException();

            User u = new User();
            u.Name = name;
            u.PassSalt = PasswordHelper.GetNewSalt();
            u.PassHash = PasswordHelper.HashPass(password, u.PassSalt);
            u.AuthToken = "*";
            u.LoginDate = DateTime.Now;
            db.Users.Add(u);

            Project p = new Project();
            p.Name = "Default Project";
            p.OwnerId = u.Id;
            p.IsDefault = true;
            db.Projects.Add(p);

            db.SaveChanges();
        }

    }
}
