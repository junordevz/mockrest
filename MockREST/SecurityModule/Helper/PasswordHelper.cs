﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Helper
{
    public static class PasswordHelper
    {
        public static string GetNewSalt()
        {
            RNGCryptoServiceProvider a = new RNGCryptoServiceProvider();
            byte[] buff = new byte[32];
            a.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }

        public static string HashPass(string password, string salt)
        {
            byte[] pass = new byte[password.Length * sizeof(char)];
            System.Buffer.BlockCopy(password.ToCharArray(), 0, pass, 0, pass.Length);
            byte[] sal = new byte[salt.Length * sizeof(char)];
            System.Buffer.BlockCopy(salt.ToCharArray(), 0, sal, 0, sal.Length);
            byte[] val = pass.Concat(sal).ToArray();

            return Convert.ToBase64String(new SHA512Managed().ComputeHash(val));
        }

    }
}
