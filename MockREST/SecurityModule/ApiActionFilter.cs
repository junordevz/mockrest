﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataBaseManager;
using DataBaseManager.Models;

namespace SecurityModule
{
    public class ApiActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.Headers["Auth-Token"] == null)
            {
                filterContext.Result = new HttpUnauthorizedResult("No Auth-Token!");
                return;
            }

            string token = filterContext.HttpContext.Request.Headers["Auth-Token"];

            DataBaseContext db = new DataBaseContext();
            User foundUser = db.Users.FirstOrDefault(x => x.AuthToken == token);

            if (foundUser == null)
            {
                filterContext.Result = new HttpUnauthorizedResult("Auth-Token not Found!");
                return;
            }

            if (foundUser.AuthToken == "*" || foundUser.AuthToken != token)
            {
                filterContext.Result = new HttpUnauthorizedResult("Auth-Token Mismatch!");
                return;
            }

            if (DateTime.UtcNow.Subtract(foundUser.LoginDate) > TimeSpan.FromDays(7))
            {
                filterContext.Result = new HttpUnauthorizedResult("Auth-Token Expired!");
                return;
            }
        }
    }
}
