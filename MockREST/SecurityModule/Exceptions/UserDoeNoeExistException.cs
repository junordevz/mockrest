﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Exceptions
{
    public class UserDoeNoeExistException : ApplicationException
    {
        public UserDoeNoeExistException() { }
        public UserDoeNoeExistException(string message) { }
        public UserDoeNoeExistException(string message, System.Exception inner) { }

        protected UserDoeNoeExistException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
        { }
    }
}
