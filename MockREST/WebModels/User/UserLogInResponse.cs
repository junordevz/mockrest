﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.User
{
    [Serializable]
    public class UserLogInResponse
    {
        public StatusCodes StatusCode { get; set; }

        public string AuthToken { get; set; }

        public UserLogInResponse()
        {
            StatusCode = StatusCodes.Ok;
            AuthToken = "";
        }
    }
}
