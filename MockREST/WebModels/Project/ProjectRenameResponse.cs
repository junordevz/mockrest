﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Project
{
    [Serializable]
    public class ProjectRenameResponse
    {
        public StatusCodes StatusCode { get; set; }

        public ProjectRenameResponse()
        {
            StatusCode = StatusCodes.Ok;
        }
    }
}
