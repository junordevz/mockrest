﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Project
{
    public class ProjectCreateStatus
    {
        public StatusCodes StatusCode { get; set; }

        public ProjectCreateStatus()
        {
            StatusCode = StatusCodes.Ok;
        }
    }
}
