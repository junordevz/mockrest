﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Project
{
    [Serializable]
    public class ProjectOfUserResponse
    {
        public StatusCodes StatusCode { get; set; }

        public List<ProjectElement> Elements { get; set; }

        public ProjectOfUserResponse()
        {
            StatusCode = StatusCodes.Ok;
            Elements = new List<ProjectElement>();
        }
    }
}
