﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Project
{
    public class ProjectElement
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public ProjectElement()
        {
            Id = 0;
            Name = "";
            IsDefault = false;
        }
    }
}
