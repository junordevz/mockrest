﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels
{
    [Serializable]
    public enum StatusCodes
    {
        Ok,
        InvalidParams,
        UserExists,
        UserNotFound,
        ProjectNotFound,
        EndpointNotFound,
        HistoryNotFound,
        PassNoMatch,
        JsonInvalid,
        CannotRenameDefault,
        MrTeapot
    }
}
