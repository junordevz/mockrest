﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels
{
    public class KeyValuePair
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public KeyValuePair()
        {
            Key = "";
            Value = "";
        }
    }
}
