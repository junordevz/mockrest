﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Endpoint
{
    public class EndpointCreateStatus
    {
        public StatusCodes StatusCode { get; set; }

        public string Link { get; set; }

        public EndpointCreateStatus()
        {
            StatusCode = StatusCodes.Ok;
            Link = "";
        }
    }
}
