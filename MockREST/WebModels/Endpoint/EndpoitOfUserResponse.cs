﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Endpoint
{
    [Serializable]
    public class EndpoitOfUserResponse
    {
        public StatusCodes StatusCode { get; set; }

        public List<EndpointElement> Elements { get; set; }

        public EndpoitOfUserResponse()
        {
            StatusCode = StatusCodes.Ok;
            Elements = new List<EndpointElement>();
        }
    }
}
