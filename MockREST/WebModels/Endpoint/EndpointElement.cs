﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Endpoint
{
    [Serializable]
    public class EndpointElement
    {
        public string Link { get; set; }

        public long ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string HttpMethod { get; set; }

        public string Description { get; set; }

        public bool IsEnabled { get; set; }

        public EndpointElement()
        {
            Link = "";
            ProjectId = 0;
            ProjectName = "";
            HttpMethod = "get";
            Description = "";
            IsEnabled = true;
        }
    }
}
