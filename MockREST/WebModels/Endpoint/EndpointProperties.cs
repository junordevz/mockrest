﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Endpoint
{
    [Serializable]
    public class EndpointProperties
    {
        public string Link { get; set; }

        public long ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string HttpMethod { get; set; }

        public int HttpStatus { get; set; }

        public int DelayMs { get; set; }

        public bool IsStaticContent { get; set; }

        public string Content { get; set; }

        public string Description { get; set; }

        public string AuthToken { get; set; }

        public bool IsEnabled { get; set; }

        public EndpointProperties()
        {
            Link = "";
            ProjectId = 0;
            ProjectName = "";
            HttpMethod = "get";
            HttpStatus = 0;
            DelayMs = 0;
            IsStaticContent = false;
            Content = "";
            AuthToken = "";
            IsEnabled = true;
        }
    }
}
