﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Endpoint
{
    public class EndpointSetEnabledResponse
    {
        public StatusCodes StatusCode { get; set; }

        public EndpointSetEnabledResponse()
        {
            StatusCode = StatusCodes.Ok;
        }
    }
}
