﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Endpoint
{
    public class EndpointSetDetailsResponse
    {
        public StatusCodes StatusCode { get; set; }

        public EndpointSetDetailsResponse()
        {
            StatusCode = StatusCodes.Ok;
        }
    }
}
