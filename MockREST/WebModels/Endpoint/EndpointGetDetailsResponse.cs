﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Endpoint
{
    [Serializable]
    public class EndpointGetDetailsResponse
    {
        public StatusCodes StatusCode { get; set; }

        public EndpointProperties Properties { get; set; }

        public EndpointGetDetailsResponse()
        {
            StatusCode = StatusCodes.Ok;
            Properties = new EndpointProperties();
        }
    }

}
