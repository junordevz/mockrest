﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.Endpoint
{
    [Serializable]
    public class EndpoitOfProjectResponse
    {
        public StatusCodes StatusCode { get; set; }

        public List<EndpointElement> Elements { get; set; }

        public EndpoitOfProjectResponse()
        {
            StatusCode = StatusCodes.Ok;
            Elements = new List<EndpointElement>();
        }
    }
}
