﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.History
{
    [Serializable]
    public class HistoryProperties
    {
        public DateTime TimeStamp { get; set; }

        public int HttpStatus { get; set; }

        public string RequestUrl { get; set; }

        public List<KeyValuePair> RequestHeaders { get; set; }

        public string ResponseText { get; set; }

        public List<KeyValuePair> ResponseHeaders { get; set; }

        public long ExecTime { get; set; }

        public HistoryProperties()
        {
            TimeStamp = DateTime.Now;
            HttpStatus = 200;
            RequestUrl = "";
            RequestHeaders = new List<KeyValuePair>();
            ResponseText = "";
            ResponseHeaders = new List<KeyValuePair>();
            ExecTime = 0;
        }
    }
}
