﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.History
{
    public class HistoryOfEndpointResponse
    {
        public StatusCodes StatusCode { get; set; }

        public List<HistoryEntry> Entries { get; set; }

        public HistoryOfEndpointResponse()
        {
            StatusCode = StatusCodes.Ok;
            Entries = new List<HistoryEntry>();
        }
    }

}
