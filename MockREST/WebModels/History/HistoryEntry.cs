﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.History
{
    [Serializable]
    public class HistoryEntry
    {
        public long Id { get; set; }

        public DateTime TimeStamp { get; set; }

        public int HttpStatus { get; set; }

        public long ExecTimeMs { get; set; }

        public HistoryEntry()
        {
            Id = 0;
            TimeStamp = DateTime.Now;
            HttpStatus = 200;
            ExecTimeMs = 0;
        }
    }
}
