﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebModels.History
{
    [Serializable]
    public class HistoryDetailsResponse
    {
        public StatusCodes StatusCode { get; set; }

        public HistoryProperties Properties { get; set; }

        public HistoryDetailsResponse()
        {
            StatusCode = StatusCodes.Ok;
            Properties = new HistoryProperties();
        }
    }
}
