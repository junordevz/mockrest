﻿using System.Data.Entity;
using DataBaseManager.Models;

namespace DataBaseManager
{
    public class DataBaseContext : DbContext
    {

        private const string ConnectionString = "Server=tcp:mockrest.database.windows.net,1433;Database=MockREST;User ID=mockrest@mockrest;Password=mock123rest!;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        public DataBaseContext() : base(ConnectionString)
        {
            // This is a hack to ensure that Entity Framework SQL Provider is copied across to the output folder.
            // As it is installed in the GAC, Copy Local does not work. It is required for probing.
            // Fixed "Provider not loaded" error
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<Endpoint> Endpoints { get; set; }

        public DbSet<History> Histories { get; set; }

    }
}
