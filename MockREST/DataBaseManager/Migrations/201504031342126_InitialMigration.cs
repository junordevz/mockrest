namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Endpoints",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Link = c.String(nullable: false),
                        OwnerId = c.Int(nullable: false),
                        ReturnCode = c.Int(nullable: false),
                        IsStaticContent = c.Boolean(nullable: false),
                        Content = c.String(nullable: false),
                        Delay = c.Time(nullable: false, precision: 7),
                        AuthUser = c.String(),
                        AuthPass = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.OwnerId, cascadeDelete: true)
                .Index(t => t.OwnerId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        PassSalt = c.String(nullable: false),
                        PassHash = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Histories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeStamp = c.DateTime(nullable: false),
                        EndpointId = c.Int(nullable: false),
                        Request = c.String(nullable: false),
                        Response = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Endpoints", t => t.EndpointId, cascadeDelete: true)
                .Index(t => t.EndpointId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Histories", "EndpointId", "dbo.Endpoints");
            DropForeignKey("dbo.Endpoints", "OwnerId", "dbo.Users");
            DropIndex("dbo.Histories", new[] { "EndpointId" });
            DropIndex("dbo.Endpoints", new[] { "OwnerId" });
            DropTable("dbo.Histories");
            DropTable("dbo.Users");
            DropTable("dbo.Endpoints");
        }
    }
}
