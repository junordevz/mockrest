namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHistoryStatusCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Histories", "HttpStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Histories", "HttpStatus");
        }
    }
}
