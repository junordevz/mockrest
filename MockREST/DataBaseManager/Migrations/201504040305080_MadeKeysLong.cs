namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadeKeysLong : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Histories", "EndpointId", "dbo.Endpoints");
            DropForeignKey("dbo.Endpoints", "OwnerId", "dbo.Users");
            DropIndex("dbo.Endpoints", new[] { "OwnerId" });
            DropIndex("dbo.Histories", new[] { "EndpointId" });
            DropPrimaryKey("dbo.Endpoints");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.Histories");
            AlterColumn("dbo.Endpoints", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Endpoints", "OwnerId", c => c.Long(nullable: false));
            AlterColumn("dbo.Users", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Histories", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Histories", "EndpointId", c => c.Long(nullable: false));
            AddPrimaryKey("dbo.Endpoints", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.Histories", "Id");
            CreateIndex("dbo.Endpoints", "OwnerId");
            CreateIndex("dbo.Histories", "EndpointId");
            AddForeignKey("dbo.Histories", "EndpointId", "dbo.Endpoints", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Endpoints", "OwnerId", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Endpoints", "OwnerId", "dbo.Users");
            DropForeignKey("dbo.Histories", "EndpointId", "dbo.Endpoints");
            DropIndex("dbo.Histories", new[] { "EndpointId" });
            DropIndex("dbo.Endpoints", new[] { "OwnerId" });
            DropPrimaryKey("dbo.Histories");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.Endpoints");
            AlterColumn("dbo.Histories", "EndpointId", c => c.Int(nullable: false));
            AlterColumn("dbo.Histories", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Users", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Endpoints", "OwnerId", c => c.Int(nullable: false));
            AlterColumn("dbo.Endpoints", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Histories", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.Endpoints", "Id");
            CreateIndex("dbo.Histories", "EndpointId");
            CreateIndex("dbo.Endpoints", "OwnerId");
            AddForeignKey("dbo.Endpoints", "OwnerId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Histories", "EndpointId", "dbo.Endpoints", "Id", cascadeDelete: true);
        }
    }
}
