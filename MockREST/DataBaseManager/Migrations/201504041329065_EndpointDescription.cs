namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EndpointDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Endpoints", "Description", c => c.String(defaultValue: "No Description"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Endpoints", "Description");
        }
    }
}
