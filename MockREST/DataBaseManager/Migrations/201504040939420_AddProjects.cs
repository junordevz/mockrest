namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjects : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Endpoints", "OwnerId", "dbo.Users");
            DropIndex("dbo.Endpoints", new[] { "OwnerId" });
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        OwnerId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.OwnerId, cascadeDelete: true)
                .Index(t => t.OwnerId);
            
            AddColumn("dbo.Endpoints", "ProjectId", c => c.Long(nullable: false));
            CreateIndex("dbo.Endpoints", "ProjectId");
            AddForeignKey("dbo.Endpoints", "ProjectId", "dbo.Projects", "Id", cascadeDelete: true);
            DropColumn("dbo.Endpoints", "OwnerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Endpoints", "OwnerId", c => c.Long(nullable: false));
            DropForeignKey("dbo.Endpoints", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "OwnerId", "dbo.Users");
            DropIndex("dbo.Projects", new[] { "OwnerId" });
            DropIndex("dbo.Endpoints", new[] { "ProjectId" });
            DropColumn("dbo.Endpoints", "ProjectId");
            DropTable("dbo.Projects");
            CreateIndex("dbo.Endpoints", "OwnerId");
            AddForeignKey("dbo.Endpoints", "OwnerId", "dbo.Users", "Id", cascadeDelete: true);
        }
    }
}
