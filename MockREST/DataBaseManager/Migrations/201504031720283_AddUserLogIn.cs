namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserLogIn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "AuthToken", c => c.String(nullable: false));
            AddColumn("dbo.Users", "LoginDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "LoginDate");
            DropColumn("dbo.Users", "AuthToken");
        }
    }
}
