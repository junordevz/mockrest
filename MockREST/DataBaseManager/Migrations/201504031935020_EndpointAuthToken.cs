namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EndpointAuthToken : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Endpoints", "AuthToken", c => c.String());
            DropColumn("dbo.Endpoints", "AuthUser");
            DropColumn("dbo.Endpoints", "AuthPass");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Endpoints", "AuthPass", c => c.String());
            AddColumn("dbo.Endpoints", "AuthUser", c => c.String());
            DropColumn("dbo.Endpoints", "AuthToken");
        }
    }
}
