namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetEndpointMethod : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Endpoints", "HttpMethod", c => c.String(nullable: false, defaultValue: "get"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Endpoints", "HttpMethod");
        }
    }
}
