namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EndpointEnable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Endpoints", "IsEnabled", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Endpoints", "IsEnabled");
        }
    }
}
