namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectDefaultBit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "IsDefault", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "IsDefault");
        }
    }
}
