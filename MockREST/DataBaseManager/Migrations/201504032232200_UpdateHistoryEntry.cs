namespace DataBaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateHistoryEntry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Histories", "RequestUrl", c => c.String());
            AddColumn("dbo.Histories", "RequestHeader", c => c.String());
            AddColumn("dbo.Histories", "ResponseText", c => c.String());
            AddColumn("dbo.Histories", "ResponseHeader", c => c.String());
            AddColumn("dbo.Histories", "ExecutionTime", c => c.Time(nullable: false, precision: 7));
            DropColumn("dbo.Histories", "Request");
            DropColumn("dbo.Histories", "Response");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Histories", "Response", c => c.String(nullable: false));
            AddColumn("dbo.Histories", "Request", c => c.String(nullable: false));
            DropColumn("dbo.Histories", "ExecutionTime");
            DropColumn("dbo.Histories", "ResponseHeader");
            DropColumn("dbo.Histories", "ResponseText");
            DropColumn("dbo.Histories", "RequestHeader");
            DropColumn("dbo.Histories", "RequestUrl");
        }
    }
}
