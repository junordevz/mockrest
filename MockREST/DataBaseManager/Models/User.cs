﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataBaseManager.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string PassSalt { get; set; }

        [Required]
        public string PassHash { get; set; }

        [Required]
        public string AuthToken { get; set; }

        [Required]
        public DateTime LoginDate { get; set; }
    }
}