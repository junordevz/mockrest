﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataBaseManager.Models
{
    public class Endpoint
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public long Id { get; set; }

        [Required]
        public string Link { get; set; }

        [Index]
        [Required]
        public long ProjectId { get; set; }

        [DefaultValue("get")]
        [Required]
        public string HttpMethod { get; set; }

        [DefaultValue(200)]
        [Required]
        public int ReturnCode { get; set; }

        [DefaultValue(false)]
        [Required]
        public bool IsStaticContent { get; set; }

        [DefaultValue("")]
        [Required]
        public string Content { get; set; }

        [DefaultValue("")]
        public string Description { get; set; }

        [DefaultValue(0)]
        [Required]
        public TimeSpan Delay { get; set; }

        [DefaultValue("*")]
        public string AuthToken { get; set; }

        [DefaultValue(true)]
        [Required]
        public bool IsEnabled { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }
    }
}