﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataBaseManager.Models
{
    public class History
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public long Id { get; set; }

        [Required]
        public DateTime TimeStamp { get; set; }

        [Required]
        public long EndpointId { get; set; }

        public int HttpStatus { get; set; }

        public string RequestUrl { get; set; }

        public string RequestHeader { get; set; }

        public string ResponseText { get; set; }

        public string ResponseHeader { get; set; }

        public TimeSpan ExecutionTime { get; set; }

        [ForeignKey("EndpointId")]
        public virtual Endpoint Endpoint { get; set; }
    }
}