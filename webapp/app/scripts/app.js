'use strict';

/**
 * @ngdoc overview
 * @name mockifiedApp
 * @description
 * # mockifiedApp
 *
 * Main module of the application.
 */
angular
    .module('mockifiedApp', [
        'ui.router',
        "ngStorage"
    ])

    .config(function ($stateProvider, $urlRouterProvider, ApiUrlsProvider) {


        $stateProvider

            .state('landing', {
                url: "/",
                templateUrl: "views/landing.html",
                controller: "LandingCtrl"
            })
            .state('app', {
                abstract: true,
                templateUrl: "views/template.html"
            })
            .state('app.login', {
                url: "/login",
                templateUrl: "views/sign/login.html",
                controller: "LoginCtrl"
            })
            .state('app.register', {
                url: "/login",
                templateUrl: "views/sign/register.html",
                controller: "RegisterCtrl"
            })
            .state('user', {
                abstract: true,
                templateUrl: "views/user/template.html"
            })
            .state('user.projects', {
                url: "/projects",
                templateUrl: "views/user/projects/projects.html",
                controller: "ProjectCtrl"
            })
            .state('user.endpoints-list', {
                url: "/endpoints",
                templateUrl: "views/user/endpoints/endpoint-list.html",
                controller: "EndpointListCtrl"
            })
            .state('user.endpoints-create', {
                url: "/create",
                controller: "EndpointCreateCtrl"
            })
            .state('user.endpoints-edit', {
                url: "/edit/:endpointId",
                templateUrl: "views/user/endpoints/endpoint-edit.html",
                controller: "EndpointEditCtrl"
            })
            .state('user.history', {
                url: "/history/:endpointId",
                templateUrl: "views/user/history/history.html",
                controller: "HistoryCtrl"
            })
            .state('user.history-details', {
                url: "/history-details/:historyId",
                templateUrl: "views/user/history/history-details.html",
                controller: "HistoryDetailsCtrl"
            });

        $urlRouterProvider.otherwise("/");

        ApiUrlsProvider.setHostname("mockified", "mockrest.azurewebsites.net/api");
        ApiUrlsProvider.setSecure("mockified", false);
        ApiUrlsProvider.setUrls("mockified", {
            "register": "/users/register",
            "login": "/users/login",
            "logout": "/users/logout",
            "createProject": "/projects/create",
            "renameProject": "/projects/rename",
            "userProjects": "/projects/ofuser",
            "historyList": "/history/ofendpoint",
            "historyDetails": "/history/details",
            "createEndpoint": "/endpoints/create",
            "getEndpoint": "/endpoints/getendpoint",
            "saveEndpoint": "/endpoints/setendpoint",
            "setEnable": "/endpoints/setenable",
            "userEndpoints": "/endpoints/ofuser",
            "projectEndpoints": "/endpoints/ofproject"
        });




    })

    .run(["$rootScope", "$http", "$localStorage", "UserService",
        function($rootScope, $http, $localStorage, UserService) {

            if ($localStorage.token) {
                $http.defaults.headers.common["Auth-Token"] = $localStorage.token;
                $rootScope.user = $localStorage.user;
            }

            $rootScope.logout = function () {
                UserService.logout($rootScope.user);
            };

        }]);
