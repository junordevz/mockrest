angular.module('mockifiedApp').factory("UserService",
    function ($http, ApiUrls, $localStorage, $rootScope) {

        return {
            register: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "register"), {
                    params: params
                });
            },
            login: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "login"), {
                    params: params
                }).then(function (data) {

                        $localStorage.token = data.data.AuthToken;

                        $http.defaults.headers.common["Auth-Token"] = data.data.AuthToken;
                        $rootScope.user = params.user;
                        $localStorage.user =  params.user;

                        return data;
                    });
            },
            logout: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "logout"), {
                    params: params
                })
                    .then(function () {
                        delete $rootScope.user;
                        $localStorage.$reset();
                    });
            }
        }
    }
);
