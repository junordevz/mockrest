angular.module('mockifiedApp').factory("EndpointService",
    function ($http, ApiUrls, $rootScope) {

        return {
            projectEndpoints: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "projectEndpoints"), {
                    params: params
                });
            },
            create: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "createEndpoint"), {
                    params: params
                });
            },
            getEndpoint: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "getEndpoint"), {
                    params: params
                });
            },
            setEndpoint: function (params) {
                return $http.post(ApiUrls.getUrl("mockified", "saveEndpoint"), {}, {
                    headers: {
                        'data': params
                    }
                });
            }

        }
    }
);
