angular.module('mockifiedApp').factory("ProjectService",
    function ($http, ApiUrls) {

        return {
            retrieve: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "userProjects"), {
                    params: params
                });
            },
            create: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "createProject"), {
                    params: params
                });
            }
        }
    }
);
