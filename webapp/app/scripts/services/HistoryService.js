angular.module('mockifiedApp').factory("HistoryService",
    function ($http, ApiUrls, $rootScope) {

        return {
            endpointHistory: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "historyList"), {
                    params: params
                });
            },
            historyDetails: function (params) {
                return $http.get(ApiUrls.getUrl("mockified", "historyDetails"), {
                    params: params
                });
            }

        }
    }
);
