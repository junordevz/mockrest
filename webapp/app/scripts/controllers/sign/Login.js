'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('LoginCtrl', function ($scope, UserService, $state) {


        $scope.login = function() {
            $scope.formError = {
                invalid: false,
                userNotExists: false
            };

            var loginData = angular.copy($scope.loginData);

            UserService.login(loginData).then(function(data) {
                console.log(data.data.StatusCode);
                if(data.data.StatusCode === 1) {
                    $scope.formError.invalid = true;
                } else if (data.data.StatusCode === 3) {
                    $scope.formError.userNotExists = true;
                } else {
                    $state.go('user.endpoints-list');
                }
            });
        }


  });
