'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('RegisterCtrl', function ($scope, UserService, $state) {

        $scope.newUser = {};

        $scope.createUser = function() {

            $scope.formError = {
                dontMatch: false,
                userExists: false
            };


            if($scope.newUser.password != $scope.newUser.password2) {
                $scope.formError.dontMatch = true;
            }

            UserService.register({
                user: $scope.newUser.user,
                password: $scope.newUser.password
            }).then(function(data) {
                if(data.data.StatusCode === 2) {
                    $scope.formError.userExists = true;
                } else {
                    if($scope.newUser.logmein) {
                        //redirect to dashboard
                    } else {
                        $state.go("app.login");
                    }
                }
            })

        }


  });
