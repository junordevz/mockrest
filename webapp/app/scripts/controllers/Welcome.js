'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:LandingCtrl
 * @description
 * # LandingCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('LandingCtrl', function ($scope, $location) {

        $scope.goToRegister = function(e) {
            e.preventDefault();
            $location.hash('register');
        }

  });
