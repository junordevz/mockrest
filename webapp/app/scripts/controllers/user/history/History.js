'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:HistoryCtrl
 * @description
 * # HistoryCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('HistoryCtrl', function ($scope, HistoryService, $stateParams) {


        var getParams = {
            link: angular.copy($stateParams.endpointId),
            skip: 0,
            take: 999
        };


        HistoryService.endpointHistory(getParams).then(function(data) {
            console.log(data.data);
            $scope.logs = data.data.Entries;
        });


    });
