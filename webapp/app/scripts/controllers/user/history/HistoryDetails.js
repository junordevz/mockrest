'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:HistoryDetailsCtrl
 * @description
 * # HistoryDetailsCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('HistoryDetailsCtrl', function ($scope, $stateParams, HistoryService) {


        var getParams = {
            id: angular.copy($stateParams.historyId)
        };


        HistoryService.historyDetails(getParams).then(function(data) {
            console.log(data.data);
            $scope.log = data.data.Properties;
        });


  });
