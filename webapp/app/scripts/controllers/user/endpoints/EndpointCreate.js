'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:EndpointCreateCtrl
 * @description
 * # EndpointCreateCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('EndpointCreateCtrl', function ($scope, EndpointService, $rootScope, $location) {


        var params = {
            user: angular.copy($rootScope.user)
        };

        EndpointService.create(params).then(function(data) {
            var endpointId = data.data.Link;

            $location.url("/edit/" + endpointId);
        });



  });
