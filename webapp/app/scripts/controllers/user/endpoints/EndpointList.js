'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:EndpointListCtrl
 * @description
 * # EndpointListCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('EndpointListCtrl', function ($scope, ProjectService, EndpointService, $rootScope) {

        var params = {
            user: angular.copy($rootScope.user)
        };
        $scope.projects = [];

        ProjectService.retrieve(params).then(function(data) {
            var projects = data.data.Elements;

            projects.forEach(function(project){
                console.log(project);
                var projectParams = {
                    id: project.Id
                };
                EndpointService.projectEndpoints(projectParams).then(function(data) {

                    var newProject = {
                        id: project.id,
                        name: project.Name,
                        isDefault: project.IsDefault,
                        endpoints: data.data.Elements
                    };

                    $scope.projects.push(newProject);
                });
            });
        });
  });
