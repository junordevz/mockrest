'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:EndpointEditCtrl
 * @description
 * # EndpointEditCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('EndpointEditCtrl', function ($scope, $stateParams, EndpointService, ProjectService, $rootScope) {


        var baseExecuteUrl = "http://mockrest.azurewebsites.net/execute/";

        $scope.isSaved = false;

        var getParams = {
            link: angular.copy($stateParams.endpointId)
        };

        function getEndpoint() {
            EndpointService.getEndpoint(getParams).then(function(data) {
                $scope.currentEndpoint = data.data.Properties;
                $scope.currentEndpoint.DelayMs /= 1000;
                if($scope.currentEndpoint.AuthToken) {
                    $scope.securedPage = true;
                }
                $scope.currentEndpoint.IsStaticContent = $scope.currentEndpoint.IsStaticContent ? 0 : 1;

            });
        }

        function getProjects() {
            var projectParams = {
                user: angular.copy($rootScope.user)
            };
            ProjectService.retrieve(projectParams).then(function(data) {
                $scope.projects = data.data.Elements;
            });
        }

        $scope.testReq = function() {
            window.open (baseExecuteUrl + $scope.currentEndpoint.Link,'_blank',false)
        };

        $scope.updatePage = function() {
            $scope.isSaved = true;
            if(!$scope.securedPage) {
                $scope.currentEndpoint.AuthToken = null;

            }

            $scope.currentEndpoint.IsStaticContent = $scope.currentEndpoint.IsStaticContent ? 0 : 1;
            var postParams = angular.copy($scope.currentEndpoint);
            postParams.DelayMs *= 1000;
            postParams = JSON.stringify(postParams);
            EndpointService.setEndpoint(postParams).then(getEndpoint);
        };


        getProjects();
        getEndpoint();
  });
