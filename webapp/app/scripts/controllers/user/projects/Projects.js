'use strict';

/**
 * @ngdoc function
 * @name mockifiedApp.controller:ProjectCtrl
 * @description
 * # ProjectCtrl
 * Controller of the mockifiedApp
 */
angular.module('mockifiedApp')
  .controller('ProjectCtrl', function ($scope, ProjectService, $rootScope) {

        $scope.showAdd = false;

        var currentUser = angular.copy($rootScope.user);

        $scope.addNow = function() {
            $scope.showAdd = true;
        };


        function getProjects() {
            var projectParams = {
                user: currentUser
            };
            ProjectService.retrieve(projectParams).then(function(data) {
                $scope.projects = data.data.Elements;
            });
        }

        $scope.addProject = function() {
            var params = {
                user: currentUser,
                name: $scope.newProject
            };

            ProjectService.create(params).then(function(data) {
                getProjects();
            });
            $scope.newProject = '';
            $scope.showAdd = false;
        };

        getProjects();

  });
