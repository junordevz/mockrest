angular.module('mockifiedApp').filter('reverse', function() {
    return function(items) {
        var reversed = items;
        if(items) {
            reversed = items.slice().reverse();
        }
        return reversed;
    };
});